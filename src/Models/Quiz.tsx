import Question from "./Question";


export default interface Quiz {
    id: number,
    name: string,
    uploaded: string,
    questions: Question[],
}