export default interface Option {
    id: number,
    value: string,
    isRight: boolean,
}