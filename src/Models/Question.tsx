import Option from "./Option"

export default interface Question {
    id: number,
    value: string,
    options: Option[],
}