import { Component, ReactNode } from "react";
import { WithRouter, WithRouterProps } from "../Utils/WithRouter";
import QuizModel from '../Models/Quiz';
import { Container } from "react-bootstrap";
import QuizComplete from "../Components/QuizComplete";
import QuizOnGoing from "../Components/QuizOnGoing";

interface IProps {}

interface IState {
    isLoaded: boolean,
    error: Error | null,
    quiz: QuizModel | null,
    currentQuestion: number,
    answers: number[],
    quizComplete: boolean,
}

class Quiz extends Component<WithRouterProps<IProps>, IState>
{
    constructor(props: WithRouterProps<IProps>)
    {
        super(props);

        this.state = {
            isLoaded: false,
            error: null,
            quiz: null, 
            currentQuestion: 0,
            answers: [],
            quizComplete: false,
        }

        this.quizCompleted = this.quizCompleted.bind(this);
    }

    public componentDidMount(): void {
        let quizId = this.props.params["quizId"]
        fetch("/api/v1/quiz/" + quizId)
        .then(response => response.json())
        .then((quiz: QuizModel) => {
            this.setState({
                isLoaded: true,
                error: null,
                quiz: quiz,
            })
        })
        .catch(error => {
            this.setState({
                isLoaded: false,
                error: error,
            });
        });
    }

    private quizCompleted(answers: number[])
    {
        this.setState({
            quizComplete: true,
            answers: answers,
        })
    }

    public render(): ReactNode {
        if(this.state.error != null)
            return (<Container>
                {this.state.error.message}
            </Container>)
        if(!this.state.isLoaded)
            return (<Container>
                <p>Loading ...</p>
            </Container>)
        return (
            <Container>
                <h1>{this.state.quiz!.name}</h1>
                {this.state.quizComplete ?
                <QuizComplete quiz={this.state.quiz!} answers={this.state.answers} /> :
                <QuizOnGoing quiz={this.state.quiz!} quizCompleteCB={this.quizCompleted}/>}
            </Container>
        );
    }
}


export default WithRouter(Quiz);