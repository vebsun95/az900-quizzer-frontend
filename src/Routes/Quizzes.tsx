import { Component, ReactNode } from "react";
import { WithRouter, WithRouterProps } from "../Utils/WithRouter";
import Quiz from '../Models/Quiz'
import { Button, Card, Container } from "react-bootstrap";
import AzureLogo from '../assets/azure-logo.png'

interface IProps { }

interface IState {
    isLoaded: boolean,
    error: Error | null,
    quizzes: Quiz[],
}

class Quizzes extends Component<WithRouterProps<IProps>, IState>
{
    constructor(props: WithRouterProps<IProps>) {
        super(props);

        this.state = {
            isLoaded: false,
            error: null,
            quizzes: [],
        }

        this.renderQuizCard = this.renderQuizCard.bind(this);
    }

    public componentDidMount(): void {
        fetch("/api/v1/quiz")
        .then(response => response.json())
        .then((quizzes: Quiz[]) => this.setState({ quizzes: quizzes, isLoaded: true, error: null }))
        .catch(error => this.setState({ isLoaded: false, error: error }));
    }

    private renderQuizCard(quiz: Quiz) {
        return (<Card style={{ width: '18rem', borderColor: "tomato" }} key={quiz.id} >
            <Card.Img variant="top" src={AzureLogo} width={128} height={128} />
            <Card.Body>
                <Card.Title>{quiz.name}</Card.Title>
                <Button onClick={() => {this.props.navigate("/quiz/" + quiz.id)}} variant="primary">Take Quiz</Button>
            </Card.Body>
        </Card>)
    }


    public render(): ReactNode {
        if (!this.state.isLoaded)
            return (<Container>
                <p>Loading ....</p>
            </Container>)
        if (this.state.error != null)
            return (<Container>
                <p>{this.state.error.message}</p>
            </Container>)
        return (
            <Container>
                {this.state.quizzes.map(this.renderQuizCard)}
            </Container>
        );
    }
}

export default WithRouter(Quizzes);