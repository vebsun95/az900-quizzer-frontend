import './App.css'
import { Container, Nav } from 'react-bootstrap'
import { Outlet, useNavigate } from 'react-router-dom'

function App() {
  const navigation = useNavigate();

  return (
    <Container fluid >
      <Nav activeKey="/" onSelect={selectedKey => navigation(selectedKey!)} as="ul">
        <Nav.Item as="li">
          <Nav.Link eventKey="/quiz">Quizzes</Nav.Link>
        </Nav.Item>
      </Nav>
      <Container className='app'>
        <Outlet />
      </Container>
    </Container>
  )
}

export default App
