import { Component, ReactNode } from "react";
import { Button, Container, InputGroup } from "react-bootstrap";
import Question from "../Models/Question";

type QuizQuestionCallBack = (optionSelected: number) => void;

interface IProp
{
    question: Question,
    buttonLabel: string,
    cb: QuizQuestionCallBack,
}

interface IState {
    checked: boolean[],
}

class QuizQuestion extends Component<IProp, IState>
{
    constructor(props: IProp)
    {
        super(props);
        this.state = {
            checked: Array(props.question.options.length).fill(false),
        }
    }

    public componentDidUpdate(prevProps: Readonly<IProp>, prevState: Readonly<IState>, snapshot?: any): void {
        if(prevProps.question.id != this.props.question.id)
            this.setState({checked: Array(this.props.question.options.length).fill(false)});
    }

    private checkBoxClicked(i: number) {
        let newChecked = Array(this.state.checked.length).fill(false);
        newChecked[i] = true;
        this.setState({checked: newChecked});
    }

    public render(): ReactNode {
        return (   
            <Container>
                <p>{this.props.question.value}</p>
                {this.props.question.options.map((option, index) => (
                <InputGroup key={index}>
                    <input type="checkbox" checked={this.state.checked[index]} onChange={() => {this.checkBoxClicked(index)}}/>
                    <InputGroup.Text>{option.value}</InputGroup.Text>
                </InputGroup>))}
                <Button onClick={() => {this.props.cb(this.state.checked.indexOf(true))}}>{this.props.buttonLabel}</Button>
            </Container>
        );
    }
}

export default QuizQuestion;