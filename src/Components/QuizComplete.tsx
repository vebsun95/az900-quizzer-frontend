import { Component, ReactNode } from "react"
import { Container, InputGroup } from "react-bootstrap";
import Option from "../Models/Option";
import Question from "../Models/Question";
import Quiz from "../Models/Quiz";
import '../Style/QuizComplete.css'

interface IProps {
    quiz: Quiz,
    answers: number[],
}

interface IState {

}

class quizComplete extends Component<IProps, IState> {

    public constructor(props: IProps) {
        super(props);
        this.renderOption = this.renderOption.bind(this);
        this.renderQuestion = this.renderQuestion.bind(this);
    }

    private renderOption(option: Option, index: number, questionIndex: number) {
        let className = "";
        if(this.props.answers[questionIndex] == index && !option.isRight)
            className = "wrong";
        if(option.isRight)
            className = "right";
        let isChecked = this.props.answers[questionIndex] === index
        return (<Container key={index} className={className}>
            <InputGroup key={index}>
                <input type="checkbox" checked={isChecked} readOnly />
                <InputGroup.Text>{option.value}</InputGroup.Text>
            </InputGroup>
        </Container>)

    }

    private renderQuestion(question: Question, questionIndex: number) {
        return (<Container key={questionIndex}>
            <p>{question.value}</p>
            {question.options.map((option, optionIndex) => this.renderOption(option, optionIndex, questionIndex))}
        </Container>)
    }

    public render(): ReactNode {
        return (
            <Container>
                {this.props.quiz.questions.map(this.renderQuestion)}
            </Container>
        )
    }
}

export default quizComplete;