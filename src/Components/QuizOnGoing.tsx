import { Component, ReactNode } from "react";
import { Container } from "react-bootstrap";
import Quiz from "../Models/Quiz";
import QuizQuestion from './QuizQuestion';

type QuizCompleteCB = (answers: number[]) => void;

interface IProps {
    quiz: Quiz,
    quizCompleteCB: QuizCompleteCB,
}

interface IState {
    answers: number[],
    currentQuestion: number,
}

class QuizOnGoing extends Component<IProps, IState>
{

    public constructor(props: IProps) {
        super(props);
        
        this.state = {
            answers: [],
            currentQuestion: 0,
        }

        this.questionAnswered = this.questionAnswered.bind(this);
    }

    private questionAnswered(optionSelected: number)
    {
        this.state.answers.push(optionSelected);
        if(this.state.currentQuestion < this.props.quiz!.questions.length - 1) {
            let currentQuestion = this.state.currentQuestion + 1;
            this.setState({currentQuestion: currentQuestion});
        }
        else {
            this.props.quizCompleteCB(this.state.answers);
        }
    }

    public render(): ReactNode {
        let currentQuestion = this.props.quiz!.questions[this.state.currentQuestion];
        let buttonLabel = this.state.currentQuestion < this.props.quiz!.questions.length - 1? "Submit answer" : "Finish quiz";
        return (
            <QuizQuestion question={currentQuestion} cb={this.questionAnswered} buttonLabel={buttonLabel}/>
        );
    }

}

export default QuizOnGoing;